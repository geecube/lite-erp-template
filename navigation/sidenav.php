<!-- START SIDEBAR-->
<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="admin-block d-flex">
            <div>
                <img src="../../assets/img/admin-avatar.png" width="45px" />
            </div>
            <div class="admin-info">
                <div class="font-strong">John Doe</div><small>Administrator</small></div>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="active" href="../../index.php"><i class="sidebar-item-icon fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li class="heading">FEATURES</li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-book"></i>
                    <span class="nav-label">Financial Management</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="#">Accounts Payable</a>
                    </li>
                    <li>
                        <a href="#">Accounts Receivable</a>
                    </li>
                    <li>
                        <a href="#">Fixed Asset</a>
                    </li>
                    <li>
                        <a href="#">General Ledger</a>
                    </li>
                    <li>
                        <a href="#">Project Billing</a>
                    </li>
                    <li>
                        <a href="#">Project Costing</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-money"></i>
                    <span class="nav-label">Sales &amp; Marketing</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li> 
                        <a href="../Sales/estimates-list.php">Estimates</a>
                    </li>
                    <li>
                        <a href="../Sales/sales-order-list.php">Sales Orders</a>
                    </li>
                    <li>
                        <a href="../Sales/invoice-list.php">Sales Invoices</a>
                    </li>
                    <li>
                        <a href="../Sales/payments-received-list.php">Payments Received</a>
                    </li>
                    <li>
                        <a href="../Sales/retainer-list.php">Retainer Invoices</a>
                    </li>
                    <li>
                        <a href="../Sales/recurring-invoice-list.php">Recurring Invoices</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-table"></i>
                    <span class="nav-label">Inventory</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="#">Inventory Control</a>
                    </li>
                    <li>
                        <a href="#">Master Units</a>
                    </li>
                    <li>
                        <a href="#">Stock Utilization Report</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-bar-chart"></i>
                    <span class="nav-label">CRM</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="#">Communication History</a>
                    </li>
                    <li>
                        <a href="#">Details of Purchases</a>
                    </li>
                    <li>
                        <a href="#">Calls</a>
                    </li>
                    <li>
                        <a href="#">Meetings</a>
                    </li>
                    <li>
                        <a href="#">Contract Duration</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-users"></i>
                    <span class="nav-label">Human Resources</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="../HR/nav-HR-data-entry.php">Data Entry</a>
                    </li>
                    <li>
                        <a href="../HR/nav-HR-timekeeping.php">Timekeeping</a>
                    </li>
                    <li>
                        <a href="../HR/nav-HR-projects.php">Projects</a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="nav-label">Recruitment</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-3-level collapse">
                            <li>
                                <a href="../HR/HR-recruitment-applicants-list.php">Applicants</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="../HR/nav-HR-payroll.php">Payroll</a>
                    </li>
                    <li>
                        <a href="../HR/nav-HR-reports.php">Reports</a>
                    </li>
                    <li>
                        <a href="../HR/nav-HR-maintainables.php">Maintainables</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-shopping-cart"></i>
                    <span class="nav-label">Purchases</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="../Purchases/expenses-list.php">Expenses</a>
                    </li>
                    <li>
                        <a href="../Purchases/recurring-expense-list.php">Recurring Expenses</a>
                    </li>
                    <li>
                        <a href="../Purchases/orders-list.php">Purchase Orders</a>
                    </li>
                    <li>
                        <a href="../Purchases/bills-list.php">Bills</a>
                    </li>
                    <li>
                        <a href="../Purchases/recurring-bills-list.php">Recurring Bills</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-link"></i>
                    <span class="nav-label">SCM</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="#">Sales &amp; Distribution</a>
                    </li>
                    <li>
                        <a href="#">Inventory</a>
                    </li>
                    <li>
                        <a href="#">Purchasing</a>
                    </li>
                    <li>
                        <a href="#">Forecasting &amp; Planning</a>
                    </li>
                    <li>
                        <a href="#">Point of Sale</a>
                    </li>
                    <li>
                        <a href="#">eCommerce</a>
                    </li>
                </ul>
            </li>
            <li class="heading">Others</li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-envelope"></i>
                    <span class="nav-label">Manufacturing</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="#">BOM &amp; Formula</a>
                    </li>
                    <li>
                        <a href="#">Routing &amp; Recipe</a>
                    </li>
                    <li>
                        <a href="#">Cost Management</a>
                    </li>
                    <li>
                        <a href="#">Quality</a>
                    </li>
                    <li>
                        <a href="#">Process Manufacturing</a>
                    </li>
                    <li>
                        <a href="#">Manufacturing Execution System</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-user"></i>
                    <span class="nav-label">Admin Task</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="../Admin/user-accounts.php">User Accounts</a>
                    </li>
                    <li>
                        <a href="../Admin/nav-Admin-Roles.php">Roles &amp; Permissions</a>
                    </li>
                    <li>
                        <a href="../Maintainables/nav-Maintainables.php">Maintainables</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<!-- END SIDEBAR-->


<!-- <div id="page">
    <div class="logo"></div>
    <ul id="navigation">
        <li><a href="">Item 1</a></li>
        <li><a href="">Item 2</a></li>
        <li><a href="">Item 3</a></li>
        <li><a href="">Item 4</a></li>
        <li><a href="">Item 5</a></li>
    </ul>
</div>

#page>div.logo+ul#navigation>li*5>a{Item $} -->



