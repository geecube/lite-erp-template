jQuery(document).ready(function ($) {

    $('#example-table').DataTable({
        pageLength: 10,
        responsive: true
        //"ajax": './assets/demo/data/table_data.json',
        /*"columns": [
            { "data": "name" },
            { "data": "office" },
            { "data": "extn" },
            { "data": "start_date" },
            { "data": "salary" }
        ]*/
    });

    $('.addDatePicker-line').click(function() {
        var table = $(this).closest('table.dynamic'),
            clone = table.find('tbody tr:first').clone();
            clone.find('select,textarea,input:not([type=hidden])')
            .val('');

        clone.find('.clear').html('')
        clone.find('[type=hidden]').remove('')
        clone.appendTo(table.find('tbody'))
    });

    $('table.dynamic').on('click', '.remove-line', function () {
        var table = $(this).closest('table.dynamic'),
            tr = table.find('tbody tr')
        id = table.attr('id');
        if (tr.length === 1) {
            tr.find('select,input').val('')
            tr.find('.clear').html('')
            tr.find('[type=hidden]').remove('');
        } else {
            $(this).closest('tr').remove();
        }
        $('#' + id).trigger('table:changed');
    });

    $(function() {
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
    });
    
});