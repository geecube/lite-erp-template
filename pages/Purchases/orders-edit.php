<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-heading">
            <h1 class="page-title">Purchase Orders</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="orders-list.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
        <div class="page-content fade-in-up">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Edit Order</div>
                            <div class="ibox-tools">
                                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-body">
                            <form>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Vendor Name</label>
                                            <input class="form-control" type="text" value="Test">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>P.O Number</label>
                                            <input class="form-control" type="text" value="PO-0000">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Reference Number</label>
                                            <input class="form-control" type="text" value="000000">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Date</label>
                                            <input class="form-control" type="text" value="0000-00-00">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Delivery Date</label>
                                            <input class="form-control" type="text" value="0000-00-00">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5 class="text-uppercase text-primary">
                                            <span>Item Details</span>
                                        </h5>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-8">
                                        <table class="table table-striped dynamic mb-0" id="schedule-table">
                                            <thead class="bg-primary text-light">
                                                <th>Name</th>
                                                <th>Quantity</th>
                                                <th>Rate</th>
                                                <th></th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="text" class="form-control" value="Test">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="0">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="0.00">
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger remove-line btn-sm"><i class="fa fa-times"></i></button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3">
                                                        <button type="button" class="btn btn-success btn-sm addDatePicker-line"><i class="fa fa-plus"></i> New line</button>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Sub Total</label>
                                            <input class="form-control" type="text" value="0.00" readonly>
                                        </div>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Shipping Charges</label>
                                            <input class="form-control" type="text" value="0.00">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Total</label>
                                            <input class="form-control" type="text" value="0.00" readonly>
                                        </div>
                                    </div>
                                </div>  
                                <hr>
                                <div class="form-group">
                                    <button class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        
<?php include '../../layouts/footer.php' ?>