<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-heading">
            <h1 class="page-title">Account Types</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="account-types-list.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
        <div class="page-content fade-in-up">
            <div class="row">
                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Edit Account Type</div>
                            <div class="ibox-tools">
                                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-body">
                            <form>
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" type="text" value="Test">
                                </div>
                                <div class="form-group">
                                    <label>Section</label>
                                    <select class="form-control">
                                        <option>Select Section</option>
                                        <option selected="">Assets</option>
                                        <option>Liability</option>
                                        <option>Equity</option>
                                        <option>Income</option>
                                        <option>Expense</option>
                                    </select>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <button class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        
<?php include '../../layouts/footer.php' ?>