<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-content fade-in-up">
	    	<br>
	    	<div class="row">
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="contacts-list.php" class="lg"><i class="fa fa-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Contacts</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-10">
	    				<center><a href="currency-list.php" class="lg"><i class="fa fa-dollar lg"></i></a></center>
	    			</div>
	    			<div class="col-md-10">
			    		<center><h4>Currencies</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-10">
	    				<center><a href="tax-list.php" class="lg"><i class="fa fa-percent lg"></i></a></center>
	    			</div>
	    			<div class="col-md-10">
			    		<center><h4>Taxes</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    	<br>
	    	<div class="row">
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="account-types-list.php" class="lg"><i class="fa fa-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Account Types</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-10">
	    				<center><a href="HR-roles-list.php" class="lg"><i class="fa fa-users lg"></i></a></center>
	    			</div>
	    			<div class="col-md-10">
			    		<center><h4>Vacant</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-10">
	    				<center><a href="#" class="lg"><i class="fa fa-shopping-cart lg"></i></a></center>
	    			</div>
	    			<div class="col-md-10">
			    		<center><h4>Vacant</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>