<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-heading">
            <h1 class="page-title">User Accounts</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="../../index.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
                &emsp;
                <a href="user-accounts-add.php" class="btn btn-success btn-md"><i class="fa fa-plus"></i> Create New User</a>
            </ol>
        </div>
        <div class="page-content fade-in-up">
            <div class="row">
                <div class="col-md-10">
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Accounts List</div>
                            <div class="ibox-tools">
                                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-body table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="branch-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Type</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        
<?php include '../../layouts/footer.php' ?>
<script type="text/javascript">
    jQuery(document).ready(function ($) {

        $('#branch-table').DataTable({
            pageLength: 10,
            "ajax": '../../assets/data/Admin/table_users.json',
            "columns": [
                { "data": "name" },
                { "data": "username" },
                { "data": "type" },
                { "data": "role" },
                { "data": "status" },
                { "defaultContent": "<a href=\"user-accounts-edit.php\" class=\"btn btn-outline-info btn-sm\" \/><i class=\"ti-pencil\"></i> Edit</a>" +    ' ' + "<a href=\"#\" class=\"btn btn-outline-danger btn-sm\" \/><i class=\"ti-trash\"></i> Delete</a>" }
            ]
        });

    })
</script>