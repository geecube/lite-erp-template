<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-content fade-in-up">
	    	<br>
	    	<div class="row">
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="#" class="lg"><i class="fa fa-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Financial Management</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-10">
	    				<center><a href="#" class="lg"><i class="fa fa-money lg"></i></a></center>
	    			</div>
	    			<div class="col-md-10">
			    		<center><h4>Sales &amp; Marketing</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-10">
	    				<center><a href="#" class="lg"><i class="fa fa-table lg"></i></a></center>
	    			</div>
	    			<div class="col-md-10">
			    		<center><h4>Inventory</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    	<br>
	    	<div class="row">
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="#" class="lg"><i class="fa fa-bar-chart lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>CRM</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-10">
	    				<center><a href="HR-roles-list.php" class="lg"><i class="fa fa-users lg"></i></a></center>
	    			</div>
	    			<div class="col-md-10">
			    		<center><h4>Human Resources</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-10">
	    				<center><a href="#" class="lg"><i class="fa fa-shopping-cart lg"></i></a></center>
	    			</div>
	    			<div class="col-md-10">
			    		<center><h4>Purchases</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    	<br>
	    	<div class="row">
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="#" class="lg"><i class="fa fa-link lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>SCM</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-10">
	    				<center><a href="#" class="lg"><i class="fa fa-envelope lg"></i></a></center>
	    			</div>
	    			<div class="col-md-10">
			    		<center><h4>Manufacturing</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>