<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-heading">
            <h1 class="page-title">User Account</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="user-accounts.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
        <div class="page-content fade-in-up">
            <div class="row">
                <div class="col-md-4">
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Create User Account</div>
                            <div class="ibox-tools">
                                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-body">
                            <form>
                                <div class="form-group">
                                    <label>Employee Name</label>
                                    <input class="form-control" type="text" value="Test">
                                </div>
                                <div class="form-group">
                                    <label>Login Type</label>
                                    <select class="form-control">
                                        <option>Select Type</option>
                                        <option>Administrator</option>
                                        <option selected value="">Standard</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input class="form-control" type="text" value="test@gmail.com">
                                </div>
                                <div class="form-group">
                                    <label>Role</label>
                                    <input class="form-control" type="text" value="Test">
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" value="" checked> Active?
                                </div>
                                <hr>
                                <div class="form-group">
                                    <button class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        
<?php include '../../layouts/footer.php' ?>