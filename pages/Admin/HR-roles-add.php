<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-heading">
            <h1 class="page-title">Human Resources Roles & Permission</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="HR-roles-list.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
        <div class="page-content fade-in-up">
            <div class="row">
                <div class="col-md-12">
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Create New Role</div>
                            <div class="ibox-tools">
                                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-body">
                            <form>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Role Name</label>
                                            <input class="form-control" type="text">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5>Select permissions you want to access</h5>
                                        <br>
                                        <ul class="nav nav-tabs tabs-bordered nav-justified">
                                            <li class="nav-item">
                                                <a href="#data-entry" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                    Data Entry
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#timekeeping" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                    Timekeeping
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#projects" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                    Projects
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#recruitment" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                    Recruitment
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#payroll" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                    Payroll
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#reports" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                    Reports
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#maintain" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                    Maintain
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="data-entry">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="card">
                                                            <div class="card-header text-center">Position</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Position<br>
                                                                <input type="checkbox">&emsp;Retrieve Position<br>
                                                                <input type="checkbox">&emsp;Update Position<br>
                                                                <input type="checkbox">&emsp;Delete Position<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="card">
                                                            <div class="card-header text-center">Division</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Division<br>
                                                                <input type="checkbox">&emsp;Retrieve Division<br>
                                                                <input type="checkbox">&emsp;Update Division<br>
                                                                <input type="checkbox">&emsp;Delete Division<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="card">
                                                            <div class="card-header">Branch</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Branch<br>
                                                                <input type="checkbox">&emsp;Retrieve Branch<br>
                                                                <input type="checkbox">&emsp;Update Branch<br>
                                                                <input type="checkbox">&emsp;Delete Branch<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="card">
                                                            <div class="card-header">Department</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Department<br>
                                                                <input type="checkbox">&emsp;Retrieve Department<br>
                                                                <input type="checkbox">&emsp;Update Department<br>
                                                                <input type="checkbox">&emsp;Delete Department<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="card">
                                                            <div class="card-header">Employee</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Employee<br>
                                                                <input type="checkbox">&emsp;Retrieve Employee<br>
                                                                <input type="checkbox">&emsp;Update Employee<br>
                                                                <input type="checkbox">&emsp;Delete Employee<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="timekeeping">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Timelogs</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Timelogs<br>
                                                                <input type="checkbox">&emsp;Retrieve Timelogs<br>
                                                                <input type="checkbox">&emsp;Update Timelogs<br>
                                                                <input type="checkbox">&emsp;Delete Timelogs<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Undertime Logs</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Undertime Logs<br>
                                                                <input type="checkbox">&emsp;Retrieve Undertime Logs<br>
                                                                <input type="checkbox">&emsp;Update Undertime Logs<br>
                                                                <input type="checkbox">&emsp;Delete Undertime Logs<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Overtime Logs</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Overtime Logs<br>
                                                                <input type="checkbox">&emsp;Retrieve Overtime Logs<br>
                                                                <input type="checkbox">&emsp;Update Overtime Logs<br>
                                                                <input type="checkbox">&emsp;Delete Overtime Logs<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Leave Requests</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Leave Requests<br>
                                                                <input type="checkbox">&emsp;Retrieve Leave Requests<br>
                                                                <input type="checkbox">&emsp;Update Leave Requests<br>
                                                                <input type="checkbox">&emsp;Delete Leave Requests<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Business Travels</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Business Travels<br>
                                                                <input type="checkbox">&emsp;Retrieve Business Travels<br>
                                                                <input type="checkbox">&emsp;Update Business Travels<br>
                                                                <input type="checkbox">&emsp;Delete Business Travels<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="projects">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Project Master List</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Master List<br>
                                                                <input type="checkbox">&emsp;Retrieve Master List<br>
                                                                <input type="checkbox">&emsp;Update Master List<br>
                                                                <input type="checkbox">&emsp;Delete Master List<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Project Assignment</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Assignment<br>
                                                                <input type="checkbox">&emsp;Retrieve Assignment<br>
                                                                <input type="checkbox">&emsp;Update Assignment<br>
                                                                <input type="checkbox">&emsp;Delete Assignment<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="recruitment">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Applicants</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Applicants<br>
                                                                <input type="checkbox">&emsp;Retrieve Applicants<br>
                                                                <input type="checkbox">&emsp;Update Applicants<br>
                                                                <input type="checkbox">&emsp;Delete Applicants<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="payroll">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Loans</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Loans<br>
                                                                <input type="checkbox">&emsp;Retrieve Loans<br>
                                                                <input type="checkbox">&emsp;Update Loans<br>
                                                                <input type="checkbox">&emsp;Delete Loans<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Deductions</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Applicants<br>
                                                                <input type="checkbox">&emsp;Retrieve Applicants<br>
                                                                <input type="checkbox">&emsp;Update Applicants<br>
                                                                <input type="checkbox">&emsp;Delete Applicants<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Cash Advance</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Applicants<br>
                                                                <input type="checkbox">&emsp;Retrieve Applicants<br>
                                                                <input type="checkbox">&emsp;Update Applicants<br>
                                                                <input type="checkbox">&emsp;Delete Applicants<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Generate Payroll</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Payroll<br>
                                                                <input type="checkbox">&emsp;Retrieve Payroll<br>
                                                                <input type="checkbox">&emsp;Update Payroll<br>
                                                                <input type="checkbox">&emsp;Delete Payroll<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Generate Payslip</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Payslip<br>
                                                                <input type="checkbox">&emsp;Retrieve Payslip<br>
                                                                <input type="checkbox">&emsp;Update Payslip<br>
                                                                <input type="checkbox">&emsp;Delete Payslip<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="reports">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Plantilla</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Plantilla<br>
                                                                <input type="checkbox">&emsp;Retrieve Plantilla<br>
                                                                <input type="checkbox">&emsp;Update Plantilla<br>
                                                                <input type="checkbox">&emsp;Delete Plantilla<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Absent Report</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Absent Report<br>
                                                                <input type="checkbox">&emsp;Retrieve Absent Report<br>
                                                                <input type="checkbox">&emsp;Update Absent Report<br>
                                                                <input type="checkbox">&emsp;Delete Absent Report<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Late Report</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Late Report<br>
                                                                <input type="checkbox">&emsp;Retrieve Late Report<br>
                                                                <input type="checkbox">&emsp;Update Late Report<br>
                                                                <input type="checkbox">&emsp;Delete Late Report<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Application Report</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Application Report<br>
                                                                <input type="checkbox">&emsp;Retrieve Application Report<br>
                                                                <input type="checkbox">&emsp;Update Application Report<br>
                                                                <input type="checkbox">&emsp;Delete Application Report<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="maintain">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Benifits</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Benifits<br>
                                                                <input type="checkbox">&emsp;Retrieve Benifits<br>
                                                                <input type="checkbox">&emsp;Update Benifits<br>
                                                                <input type="checkbox">&emsp;Delete Benifits<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Holidays</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Holidays<br>
                                                                <input type="checkbox">&emsp;Retrieve Holidays<br>
                                                                <input type="checkbox">&emsp;Update Holidays<br>
                                                                <input type="checkbox">&emsp;Delete Holidays<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Forms</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Forms<br>
                                                                <input type="checkbox">&emsp;Retrieve Forms<br>
                                                                <input type="checkbox">&emsp;Update Forms<br>
                                                                <input type="checkbox">&emsp;Delete Forms<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="card">
                                                            <div class="card-header">Schedule Type</div>
                                                            <div class="card-body">
                                                                <input type="checkbox">&emsp;All<br>
                                                                <input type="checkbox">&emsp;Create Schedule Type<br>
                                                                <input type="checkbox">&emsp;Retrieve Schedule Type<br>
                                                                <input type="checkbox">&emsp;Update Schedule Type<br>
                                                                <input type="checkbox">&emsp;Delete Schedule Type<br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <button class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        
<?php include '../../layouts/footer.php' ?>