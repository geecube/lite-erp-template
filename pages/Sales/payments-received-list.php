<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-heading">
            <h1 class="page-title">Payments Received</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="../../index.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
                &emsp;
                <a href="payments-received-add.php" class="btn btn-success btn-md"><i class="fa fa-plus"></i> Create Payment</a>
            </ol>
        </div>
        <div class="page-content fade-in-up">
            <div class="row">
                <div class="col-md-12">
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Payments Received List</div>
                            <div class="ibox-tools">
                                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-body table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="payments-received-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Payment Number</th>
                                        <th>Type</th>
                                        <th>Reference Number</th>
                                        <th>Customer Name</th>
                                        <th>Invoice Number</th>
                                        <th>Mode</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <tr>
                                        <td>Laguna</td>
                                        <td></td>
                                        <td>
                                            <a href="#" class="btn btn-outline-info btn-sm">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm trash-row" href="#">
                                                <span class="ti-trash"></span> Delete
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Subic</td>
                                        <td></td>
                                        <td>
                                            <a href="#" class="btn btn-outline-info btn-sm">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm trash-row" href="#">
                                                <span class="ti-trash"></span> Delete
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Quantity Surveying-INFRA</td>
                                        <td></td>
                                        <td>
                                            <a href="#" class="btn btn-outline-info btn-sm">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm trash-row" href="#">
                                                <span class="ti-trash"></span> Delete
                                            </a>
                                        </td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        
<?php include '../../layouts/footer.php' ?>
<script type="text/javascript">
    jQuery(document).ready(function ($) {

        $('#payments-received-table').DataTable({
            pageLength: 10,
            "ajax": '../../assets/data/Sales/table_payments_received.json',
            "columns": [
                { "data": "date" },
                { "data": "payment_num" },
                { "data": "type" },
                { "data": "reference" },
                { "data": "name" },
                { "data": "invoice_num" },
                { "data": "mode" },
                { "defaultContent": "<a href=\"payments-received-edit.php\" class=\"btn btn-outline-info btn-sm\" \/><i class=\"ti-pencil\"></i> Edit</a>" +    ' ' + "<a href=\"#\" class=\"btn btn-outline-danger btn-sm\" \/><i class=\"ti-trash\"></i> Delete</a>" }
            ]
        });

    })
</script>