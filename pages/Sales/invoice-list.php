<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-heading">
            <h1 class="page-title">Sales Invoice</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="../../index.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
                &emsp;
                <a href="invoice-add.php" class="btn btn-success btn-md"><i class="fa fa-plus"></i> Create Invoice</a>
            </ol>
        </div>
        <div class="page-content fade-in-up">
            <div class="row">
                <div class="col-md-12">
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Invoices List</div>
                            <div class="ibox-tools">
                                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-body table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="invoice-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Estimate Number</th>
                                        <th>Order Number</th>
                                        <th>Customer Name</th>
                                        <th>Status</th>
                                        <th>Due Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <tr>
                                        <td>Laguna</td>
                                        <td></td>
                                        <td>
                                            <a href="#" class="btn btn-outline-info btn-sm">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm trash-row" href="#">
                                                <span class="ti-trash"></span> Delete
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Subic</td>
                                        <td></td>
                                        <td>
                                            <a href="#" class="btn btn-outline-info btn-sm">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm trash-row" href="#">
                                                <span class="ti-trash"></span> Delete
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Quantity Surveying-INFRA</td>
                                        <td></td>
                                        <td>
                                            <a href="#" class="btn btn-outline-info btn-sm">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm trash-row" href="#">
                                                <span class="ti-trash"></span> Delete
                                            </a>
                                        </td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        
<?php include '../../layouts/footer.php' ?>
<script type="text/javascript">
    jQuery(document).ready(function ($) {

        $('#invoice-table').DataTable({
            pageLength: 10,
            "ajax": '../../assets/data/Sales/table_invoice.json',
            "columns": [
                { "data": "date" },
                { "data": "invoice_num" },
                { "data": "order_num" },
                { "data": "name" },
                { "data": "status" },
                { "data": "due_date" },
                { "defaultContent": "<a href=\"invoice-edit.php\" class=\"btn btn-outline-info btn-sm\" \/><i class=\"ti-pencil\"></i> Edit</a>" +    ' ' + "<a href=\"#\" class=\"btn btn-outline-danger btn-sm\" \/><i class=\"ti-trash\"></i> Delete</a>" }
            ]
        });

    })
</script>