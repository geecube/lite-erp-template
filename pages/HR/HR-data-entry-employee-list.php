<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Employees</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="nav-HR-data-entry.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
                &emsp;
                <a href="HR-data-entry-employee-add.php" class="btn btn-success btn-md"><i class="fa fa-plus"></i> Add Employee</a>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	        <div class="row">
	            <div class="col-md-12">
	            	<div class="ibox">
		                <div class="ibox-head">
		                    <div class="ibox-title">Employee List</div>
		                    <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                        </div>
		                </div>
		                <div class="ibox-body table-responsive">
		                    <table class="table table-striped table-bordered table-hover" id="employee-table" cellspacing="0" width="100%">
		                        <thead>
		                            <tr>
		                                <th>ID No.</th>
		                                <th>Employee Name</th>
		                                <th>Position</th>
		                                <th>Department</th>
		                                <th>Division</th>
		                                <th>Branch</th>
		                                <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <!-- <tr>
		                                <td>0001</td>
		                                <td>John Doe</td>
		                                <td></td>
		                                <td></td>
		                                <td></td>
		                                <td></td>
		                                <td>
			                            	<a href="#" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> Edit
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr>
		                            <tr>
		                                <td>0002</td>
		                                <td>Jane Doe</td>
		                                <td></td>
		                                <td></td>
		                                <td></td>
		                                <td></td>
		                                <td>
		                                	<a href="#" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> Edit
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr> -->
		                        </tbody>
		                    </table>
		                </div>
		            </div>
	            </div>
        	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {

	    $('#employee-table').DataTable({
	        pageLength: 10,
	        "ajax": '../../assets/data/HR/table_employee.json',
	        "columns": [
	            { "data": "id_no" },
	            { "data": "name" },
	            { "data": "position" },
	            { "data": "department" },
	            { "data": "division" },
	            { "data": "branch" },
	            { "defaultContent": "<a href=\"HR-data-entry-employee.php\" class=\"btn btn-outline-info btn-sm\" \/><i class=\"ti-pencil\"></i> View</a>" +	' ' + "<a href=\"#\" class=\"btn btn-outline-danger btn-sm\" \/><i class=\"ti-trash\"></i> Delete</a>" }
	        ]
	    });

	})
</script>