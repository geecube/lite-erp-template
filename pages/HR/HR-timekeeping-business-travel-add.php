<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Business Travel Form</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="HR-timekeeping-business-travel-list.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	    	<div class="row">
	            <div class="col-md-12">
	                <div class="ibox">
	                    <div class="ibox-head">
	                        <div class="ibox-title">New Business Travel</div>
	                        <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                        </div>
	                    </div>
	                    <div class="ibox-body">
	                        <form>
	                        	<div class="row">
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Employee Name</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Date Filed</label>
			                                <input class="form-control" type="date">
			                            </div>
		                        	</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
	                        		<div class="col-sm-12">
										<h5 class="text-uppercase text-primary">
											<span>Business Itinerary</span>
										</h5>
									</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
									<div class="col-sm-12 table-responsive">
										<table class="table table-striped dynamic mb-0" id="schedule-table">
											<thead class="bg-primary text-light text-center">
												<tr>
													<th colspan="2">ITINERARY</th>
													<th rowspan="2">PURPOSE *</th>
													<th rowspan="2">Representative Signature *</th>
													<th colspan="2">TIME OF</th>
													<th></th>
												</tr>
												<tr>
													<th>From *</th>
													<th>To *</th>
													<th>Departure *</th>
													<th>Return *</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<button type="button" class="btn btn-danger remove-line btn-sm"><i class="fa fa-times"></i></button>
													</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="3">
														<button type="button" class="btn btn-success btn-sm addDatePicker-line"><i class="fa fa-plus"></i> New line</button>
													</td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
								<hr>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
			                                <label>Prepared By</label>
			                                <input class="form-control" type="text">
			                            </div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
			                                <label>Approved By</label>
			                                <input class="form-control" type="date">
			                            </div>
									</div>
								</div>
								<hr>
	                            <div class="form-group">
	                                <button class="btn btn-success"><i class="fa fa-save"></i> Save</button>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>