<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Applicants</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="HR-recruitment-applicants-list.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	    	<div class="row">
	            <div class="col-md-10">
	                <div class="ibox">
	                    <div class="ibox-head">
	                        <div class="ibox-title">John Doe</div>
	                        <div class="ibox-tools">	
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                        </div>
	                    </div>
	                    <div class="ibox-body">
                        	<div class="row">
                        		<div class="col-md-4">
		                            <div class="form-group">
		                                <label><b>Applying As</b></label>
		                                <p>Quantity Surveyor</p>
		                            </div>
	                        	</div>
	                        	<div class="col-md-4">
		                            <div class="form-group">
		                                <label><b>Job Advertisement Found</b></label>
		                                <p>Jobstreet</p>
		                            </div>
	                        	</div>
                        	</div>
                        	<hr>
                        	<div class="row">
                        		<div class="col-sm-12">
									<h5 class="text-uppercase text-primary">
										<span>Personal Information</span>
									</h5>
								</div>
                        	</div>
                        	<hr>
							<div class="row">
                        		<div class="col-md-4">
		                            <div class="form-group">
		                                <label><b>Name</b></label>
		                                <p>John Doe</p>
		                            </div>
	                        	</div>
	                        	<div class="col-md-4">
		                            <div class="form-group">
		                                <label><b>Address</b></label>
		                                <p>Test street, Test, Test City, Test</p>
		                            </div>
	                        	</div>
                        	</div>		
                        	<div class="row">
                        		<div class="col-md-4">
		                            <div class="form-group">
		                                <label><b>E-mail Address</b></label>
		                               <p>johndoe@gmail.com</p>
		                            </div>
	                        	</div>
	                        	<div class="col-md-4">
		                            <div class="form-group">
		                                <label><b>Contact Number</b></label>
		                                <p>09231434156</p>
		                            </div>
	                        	</div>
	                        	<div class="col-md-4">
		                            <div class="form-group">
		                                <label><b>Date of Birth</b></label>
		                                <p>05/29/1989</p>
		                            </div>
	                        	</div>
                        	</div>
                        	<hr>
                        	<div class="row">
                        		<div class="col-sm-12">
									<h5 class="text-uppercase text-primary">
										<span>Education</span>
									</h5>
								</div>
                        	</div>
                        	<hr>
                        	<div class="row">
								<div class="col-sm-12 col-xs-8">
									<table class="table table-striped dynamic mb-0" id="schedule-table">
										<thead class="bg-primary text-light">
											<th>Educational Attainment</th>
											<th>School/Univeristy Attended</th>
											<th>Year</th>
										</thead>
										<tbody>
											<tr>
												<td>
													<p>Elementary</p>
												</td>
												<td>
													<p>USC</p>
												</td>
												<td>
													<p>2007</p>
												</td>
											</tr>
											<tr>
												<td>
													<p>Highschool</p>
												</td>
												<td>
													<p>USC</p>
												</td>
												<td>
													<p>2011</p>
												</td>
											</tr>
											<tr>
												<td>
													<p>Undergraduate</p>
												</td>
												<td>
													<p>USC</p>
												</td>
												<td>
													<p>2015</p>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<hr>
							<div class="row">
                        		<div class="col-sm-12">
									<h5 class="text-uppercase text-primary">
										<span>Previous Employment</span>
									</h5>
								</div>
                        	</div>
                        	<hr>
                        	<div class="row">
								<div class="col-sm-12 col-xs-8">
									<table class="table table-striped dynamic mb-0" id="schedule-table">
										<thead class="bg-primary text-light">
											<th>Previous Employer</th>
											<th>Previous Position</th>
											<th>Salary</th>
										</thead>
										<tbody>
											<tr>
												<td>
													<p>None</p>
												</td>
												<td>
													<p>None</p>
												</td>
												<td>
													<p>None</p>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<hr>
							<div class="row">
                        		<div class="col-sm-12">
									<h5 class="text-uppercase text-primary">
										<span>Salary</span>
									</h5>
								</div>
                        	</div>
							<hr>
							<div class="row">
                        		<div class="col-md-3">
		                            <div class="form-group">
		                                <label><b>Basic Salary</b></label>
		                                <p>None</p>
		                            </div>
	                        	</div>
	                        	<div class="col-md-3">
		                            <div class="form-group">
		                                <label><b>Allowance</b></label>
		                                <p>None</p>
		                            </div>
	                        	</div>
	                        	<div class="col-md-3">
		                            <div class="form-group">
		                                <label><b>Expected Salary</b></label>
		                                <p>20,000.00</p>
		                            </div>
	                        	</div>
	                        	<div class="col-md-3">
		                            <div class="form-group">
		                                <label><b>Negotiatied at</b></label>
		                                <p>15,000.00</p>
		                            </div>
	                        	</div>
                        	</div>
                        	<hr>
							<div class="row">
                        		<div class="col-sm-12">
									<h5 class="text-uppercase text-primary">
										<span>Others</span>
									</h5>
								</div>
                        	</div>
							<hr>
							<div class="row">
								<div class="col-md-4">
		                            <div class="form-group">
		                                <label><b>Availability for Employment</b></label>
		                                <p>07/20/2018</p>
		                            </div>
	                        	</div>
	                        	<div class="col-md-4">
		                            <div class="form-group">
		                                <label><b>Remarks</b></label>
		                                <p>None</p>
		                            </div>
	                        	</div>
							</div>
                        	<hr>
                            <div class="form-group">
                                <a href="HR-recruitment-applicant-edit.php" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                            </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>