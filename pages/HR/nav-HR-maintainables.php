<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-content fade-in-up">
	    	<br>
	    	<div class="row">
	    		<div class="col-md-6">
	    			<div class="col-md-12">
	    				<center><a href="HR-maintainables-benefits-list.php" class="lg"><i class="fa fa-archive lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Benefits</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-6">
	    			<div class="col-md-12">
	    				<center><a href="HR-maintainables-holidays-list.php" class="lg"><i class="fa fa-calendar lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Holidays</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    	<br><br>
	    	<div class="row">
	    		<div class="col-md-6">
	    			<div class="col-md-12">
	    				<center><a href="HR-maintainables-schedule-type-list.php" class="lg"><i class="fa fa-calendar lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Schedule Type</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-6">
	    			<div class="col-md-12">
	    				<center><a href="HR-maintainables-forms-list.php" class="lg"><i class="fa fa-wpforms lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Forms</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>