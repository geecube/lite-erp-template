<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-heading">
            <h1 class="page-title">Project Master List</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="HR-projects-master-list.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
        <div class="page-content fade-in-up">
            <div class="row">
                <div class="col-md-6">
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Create Project</div>
                            <div class="ibox-tools">
                                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-body">
                            <form>
                                <div class="row">
	                        		<div class="col-md-6">
			                            <div class="form-group">
			                                <label>Reference Number</label>
			                                <input class="form-control" type="text" value="test">
			                            </div>
		                        	</div>
		                        	<div class="col-md-6">
			                            <div class="form-group">
			                                <label>Title</label>
			                                <input class="form-control" type="text" value="0000-00-00">
			                            </div>
		                        	</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-12">
			                            <div class="form-group">
			                                <label>Description</label>
			                                <textarea class="form-control">Test</textarea>
			                            </div>
		                        	</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-12">
			                            <div class="form-group">
			                                <label>Address</label>
			                                <textarea class="form-control">Test</textarea>
			                            </div>
		                        	</div>
	                        	</div>
	                        	<hr>
                                <div class="form-group">
                                    <button class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        
<?php include '../../layouts/footer.php' ?>