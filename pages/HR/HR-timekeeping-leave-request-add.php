<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Leave Request</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="HR-timekeeping-leave-request-list.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	    	<div class="row">
	            <div class="col-md-10">
	                <div class="ibox">
	                    <div class="ibox-head">
	                        <div class="ibox-title">New Leave Request</div>
	                        <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                        </div>
	                    </div>
	                    <div class="ibox-body">
	                        <form>
	                        	<div class="row">
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Employee Name</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Date Filed</label>
			                                <input class="form-control" type="date">
			                            </div>
		                        	</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
	                        		<div class="col-sm-12">
										<h5 class="text-uppercase text-primary">
											<span>Leave Details</span>
										</h5>
									</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
									<div class="col-sm-12 col-xs-8">
										<table class="table table-striped dynamic mb-0" id="schedule-table">
											<thead class="bg-primary text-light">
												<th>Leave Type *</th>
												<th>From *</th>
												<th>To *</th>
												<th colspan="2">Reason for Leave *</th>
												<th></th>
											</thead>
											<tbody>
												<tr>
													<td>
														<select class="form-control">
															<option selected="">Select Type</option>
															<option>Vacation Leave</option>
															<option>Sick Leave</option>
															<option>Maternity/Paternity</option>
															<option>Leave of Absence</option>
															<option>Emergency</option>
														</select>
													</td>
													<td>
														<input type="date" class="form-control">
													</td>
													<td>
														<input type="date" class="form-control">
													</td>
													<td>
														<textarea class="form-control"></textarea>
													</td>
													<td>
														<button type="button" class="btn btn-danger remove-line btn-sm"><i class="fa fa-times"></i></button>
													</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="3">
														<button type="button" class="btn btn-success btn-sm addDatePicker-line"><i class="fa fa-plus"></i> New line</button>
													</td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
								<hr>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
			                                <label>Approved By</label>
			                                <input class="form-control" type="text">
			                            </div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
			                                <label>Date Approved</label>
			                                <input class="form-control" type="date">
			                            </div>
									</div>
								</div>
								<hr>
	                            <div class="form-group">
	                                <button class="btn btn-success"><i class="fa fa-save"></i> Save</button>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>