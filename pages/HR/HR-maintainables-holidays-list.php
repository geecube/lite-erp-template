<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-heading">
            <h1 class="page-title">Holiday</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="nav-HR-maintainables.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
                &emsp;
                <a href="HR-maintainables-holidays-add.php" class="btn btn-success btn-md"><i class="fa fa-plus"></i> Create Holiday</a>
            </ol>
        </div>
        <div class="page-content fade-in-up">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Holiday List</div>
                            <div class="ibox-tools">
                                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-body table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="holiday-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <tr>
                                        <td>Christmas Day</td>
                                        <td>December 25</td>
                                        <td>Legal Holiday</td>
                                        <td>
                                            <a href="#" class="btn btn-outline-info btn-sm">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm trash-row" href="#">
                                                <span class="ti-trash"></span> Delete
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Labor Day</td>
                                        <td>May 1</td>
                                        <td>Legal Holiday</td>    
                                        <td>
                                            <a href="#" class="btn btn-outline-info btn-sm">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm trash-row" href="#">
                                                <span class="ti-trash"></span> Delete
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Halloween</td>
                                        <td>November 1</td>
                                        <td>Legal Holiday</td>
                                        <td>
                                            <a href="#" class="btn btn-outline-info btn-sm">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm trash-row" href="#">
                                                <span class="ti-trash"></span> Delete
                                            </a>
                                        </td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        
<?php include '../../layouts/footer.php' ?>

<script type="text/javascript">
    jQuery(document).ready(function ($) {

        $('#holiday-table').DataTable({
            pageLength: 10,
            "ajax": '../../assets/data/HR/table_holiday.json',
            "columns": [
                { "data": "name" },
                { "data": "date" },
                { "data": "type" },
                { "defaultContent": "<a href=\"HR-maintainables-holidays-edit.php\" class=\"btn btn-outline-info btn-sm\" \/><i class=\"ti-pencil\"></i> Edit</a>" +    ' ' + "<a href=\"#\" class=\"btn btn-outline-danger btn-sm\" \/><i class=\"ti-trash\"></i> Delete</a>" }
            ]
        });

    })
</script>
