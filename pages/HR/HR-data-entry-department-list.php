<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Department</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="nav-HR-data-entry.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
                &emsp;
                <a href="HR-data-entry-department-add.php" class="btn btn-success btn-md"><i class="fa fa-plus"></i> Create Department</a>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	    	<div class="row">
	            <div class="col-md-8">
	            	<div class="ibox">
		                <div class="ibox-head">
		                    <div class="ibox-title">Department List</div>
		                    <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                        </div>
		                </div>
		                <div class="ibox-body table-responsive">
		                    <table class="table table-striped table-bordered table-hover" id="department-table" cellspacing="0" width="100%">
		                        <thead>
		                            <tr>
		                                <th>Department Name</th>
		                                <th>Branch</th>
		                                <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <!-- <tr>
		                                <td>Sales</td>
		                                <td>
		                                	<a href="#" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> Edit
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr>
		                            <tr>
		                                <td>Marketing</td>
		                                <td>
		                                	<a href="#" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> Edit
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr>
		                            <tr>
		                                <td>Maintenance & Utilities</td>
		                                <td>
		                                	<a href="#" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> Edit
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr> -->
		                        </tbody>
		                    </table>
		                </div>
		            </div>
	            </div>
        	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>

<script type="text/javascript">
    jQuery(document).ready(function ($) {

        $('#department-table').DataTable({
            pageLength: 10,
            "ajax": '../../assets/data/HR/table_department.json',
            "columns": [
                { "data": "name" },
                { "data": "branch" },
                { "defaultContent": "<a href=\"HR-data-entry-department-edit.php\" class=\"btn btn-outline-info btn-sm\" \/><i class=\"ti-pencil\"></i> Edit</a>" +    ' ' + "<a href=\"#\" class=\"btn btn-outline-danger btn-sm\" \/><i class=\"ti-trash\"></i> Delete</a>" }
            ]
        });

    })
</script>