<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Leave Request</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="nav-HR-timekeeping.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
                &emsp;
                <a href="HR-timekeeping-leave-request-add.php" class="btn btn-success btn-md"><i class="fa fa-plus"></i> Create Leave Request</a>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	        <div class="row">
	            <div class="col-md-10">
	            	<div class="ibox">
		                <div class="ibox-head">
		                    <div class="ibox-title">Leave Request List</div>
		                    <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                            <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
	                            <div class="dropdown-menu dropdown-menu-right">
	                                <a class="dropdown-item">option 1</a>
	                                <a class="dropdown-item">option 2</a>
	                            </div>
	                        </div>
		                </div>
		                <div class="ibox-body">
		                    <table class="table table-striped table-bordered table-hover" id="leave-request-table" cellspacing="0" width="100%">
		                        <thead>
		                            <tr>
		                                <th>Employee Name</th>
		                                <th>Position</th>
		                                <th>Department</th>
		                                <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <!-- <tr>
		                                <td>John Doe</td>
		                                <td></td>
		                                <td></td>
		                                <td>
		                                	<a href="#" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> Edit
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr>
		                            <tr>
		                                <td>Jane Doe</td>
		                                <td></td>
		                                <td></td>
		                                <td>
		                                	<a href="#" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> Edit
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr> -->
		                        </tbody>
		                    </table>
		                </div>
		            </div>
	            </div>
        	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>

<script type="text/javascript">
    jQuery(document).ready(function ($) {

        $('#leave-request-table').DataTable({
            pageLength: 10,
            "ajax": '../../assets/data/HR/table_leave_request.json',
            "columns": [
                { "data": "name" },
                { "data": "position" },
                { "data": "department" },
                { "defaultContent": "<a href=\"HR-timekeeping-leave-request.php\" class=\"btn btn-outline-info btn-sm\" \/><i class=\"ti-pencil\"></i> View</a>" +    ' ' + "<a href=\"#\" class=\"btn btn-outline-danger btn-sm\" \/><i class=\"ti-trash\"></i> Delete</a>" }
            ]
        });

    }) 
</script>