<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Timelogs</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="nav-HR-timekeeping.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
                &emsp;
                <a href="HR-timekeeping-timelogs-add.php" class="btn btn-success btn-md"><i class="fa fa-plus"></i> Add Timelog</a>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	        <div class="row">
	            <div class="col-md-12">
	            	<div class="ibox">
		                <div class="ibox-head">
		                    <div class="ibox-title">Timelogs List</div>
		                    <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                        </div>
		                </div>
		                <div class="ibox-body table-responsive">
		                    <table class="table table-striped table-bordered table-hover" id="timelogs-table" cellspacing="0" width="100%">
		                        <thead>
		                            <tr>
		                                <th>Date</th>
						                <th>Employee Name</th>
						                <th>Time-in</th>
						                <th>Time-out</th>
						                <th class="bg-danger text-white">Late/mins</th>
						                <th class="bg-warning text-white">Undertime/hrs</th>
						                <th class="bg-info text-white">Overtime/hrs</th>
						                <th class="bg-success text-white">Total hours</th>
						                <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <!-- <tr>
		                                <td>2018-07-16</td>
		                                <td>John Doe</td>
		                                <td>2018-07-16 15:00:00</td>
		                                <td>2018-07-16 17:00:00</td>
		                                <td>0.00</td>
		                                <td>0.00</td>
		                                <td>0.00</td>
		                                <td>2.00</td>
		                                <td>
		                                	<a href="#" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> Edit
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr>
		                            <tr>
		                                <td>2018-07-16</td>
		                                <td>Jane Doe</td>
		                                <td>2018-07-16 12:00:00</td>
		                                <td>2018-07-16 19:00:00</td>
		                                <td>0.00</td>
		                                <td>0.00</td>
		                                <td>0.00</td>
		                                <td>7.00</td>
		                                <td>
		                                	<a href="#" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> Edit
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr> -->
		                        </tbody>
		                    </table>
		                </div>
		            </div>
	            </div>
        	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {

	    $('#timelogs-table').DataTable({
	        pageLength: 10,
	        "ajax": '../../assets/data/HR/table_timelogs.json',
	        "columns": [
	            { "data": "date" },
	            { "data": "name" },
	            { "data": "time_in" },
	            { "data": "time_out" },
	            { "data": "late" },
	            { "data": "undertime" },
	            { "data": "overtime" },
	            { "data": "total" },
	            { "defaultContent": "<a href=\"#\" class=\"btn btn-outline-info btn-sm\" \/><i class=\"ti-pencil\"></i> Edit</a>" +	' ' + "<a href=\"#\" class=\"btn btn-outline-danger btn-sm\" \/><i class=\"ti-trash\"></i> Delete</a>" }
	        ]
	    });
	})
</script>
