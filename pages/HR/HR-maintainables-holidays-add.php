<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-heading">
            <h1 class="page-title">Holiday</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="HR-maintainables-holidays-list.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
        <div class="page-content fade-in-up">
            <div class="row">
                <div class="col-md-6">
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">New Holiday</div>
                            <div class="ibox-tools">
                                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-body">
                            <form>
                                <div class="row">
	                        		<div class="col-md-12">
			                            <div class="form-group">
			                                <label>Holiday Name</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-12">
			                            <div class="form-group">
			                                <label>Date</label>
			                                <input type="date" class="form-control">
			                            </div>
		                        	</div>
	                        	</div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Holiday Type</label>
                                            <select class="form-control">
                                                <option selected="">Select Type</option>
                                                <option>Legal Holiday</option>
                                                <option>Special Holiday</option>
                                                <option>Double Holiday</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
	                        	<hr>
                                <div class="form-group">
                                    <button class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        
<?php include '../../layouts/footer.php' ?>
