<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-content fade-in-up">
	    	<br>
	    	<div class="row">
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-payroll-deductions-list.php" class="lg"><i class="fa fa-address-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Deductions</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-payroll-loans-list.php" class="lg"><i class="fa fa-address-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Loans</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-payroll-generate-cash-advance-list.php" class="lg"><i class="fa fa-address-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Cash Advance</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    	<br><br>
	    	<div class="row">
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-payroll-generate-payroll.php" class="lg"><i class="fa fa-print lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Generate Payroll</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-payroll-generate-payslip.php" class="lg"><i class="fa fa-print lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Generate Payslip</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>