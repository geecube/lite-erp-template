<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-content fade-in-up">
	    	<br>
	    	<div class="row">
	    		<div class="col-md-6">
	    			<div class="col-md-12">
	    				<center><a href="#" class="lg"><i class="fa fa-users lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Plantilla</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-6">
	    			<div class="col-md-12">
	    				<center><a href="#" class="lg"><i class="fa fa-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Late Report</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    	<br><br>
	    	<div class="row">
	    		<div class="col-md-6">
	    			<div class="col-md-12">
	    				<center><a href="#" class="lg"><i class="fa fa-address-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Absent Report</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-6">
	    			<div class="col-md-12">
	    				<center><a href="#" class="lg"><i class="fa fa-address-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Application Report</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>