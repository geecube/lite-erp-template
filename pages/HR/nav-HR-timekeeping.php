<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-content fade-in-up">
	    	<br>
	    	<div class="row">
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-timekeeping-timelogs-list.php" class="lg"><i class="fa fa-address-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Timelogs</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-timekeeping-overtime-logs-list.php" class="lg"><i class="fa fa-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Overtime Logs</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-timekeeping-undertime-logs-list.php" class="lg"><i class="fa fa-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Undertime Logs</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    	<br><br>
	    	<div class="row">
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-timekeeping-leave-request-list.php" class="lg"><i class="fa fa-wpforms lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Leave Request</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-timekeeping-business-travel-list.php" class="lg"><i class="fa fa-wpforms lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Business Travel Form</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>