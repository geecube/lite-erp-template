<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-heading">
            <h1 class="page-title">Division</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="nav-HR-data-entry.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
                &emsp;
                <a href="HR-data-entry-division-add.php" class="btn btn-success btn-md"><i class="fa fa-plus"></i> Create Division</a>
            </ol>
        </div>
        <div class="page-content fade-in-up">
            <div class="row">
                <div class="col-md-8">
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Division List</div>
                            <div class="ibox-tools">
                                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-body table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="division-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Division Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <tr>
                                        <td>Fit Out</td>
                                        <th>
                                            <a href="#" class="btn btn-outline-info btn-sm">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm trash-row" href="#">
                                                <span class="ti-trash"></span> Delete
                                            </a>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>Quantity Surveying</td>
                                        <th>
                                            <a href="#" class="btn btn-outline-info btn-sm">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm trash-row" href="#">
                                                <span class="ti-trash"></span> Delete
                                            </a>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>Main</td>
                                        <th>
                                            <a href="#" class="btn btn-outline-info btn-sm">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                            <a class="btn btn-outline-danger btn-sm trash-row" href="#">
                                                <span class="ti-trash"></span> Delete
                                            </a>
                                        </th>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        
<?php include '../../layouts/footer.php' ?>

<script type="text/javascript">
    jQuery(document).ready(function ($) {

        $('#division-table').DataTable({
            pageLength: 10,
            "ajax": '../../assets/data/HR/table_division.json',
            "columns": [
                { "data": "name" },
                { "defaultContent": "<a href=\"HR-data-entry-division-edit.php\" class=\"btn btn-outline-info btn-sm\" \/><i class=\"ti-pencil\"></i> Edit</a>" +    ' ' + "<a href=\"#\" class=\"btn btn-outline-danger btn-sm\" \/><i class=\"ti-trash\"></i> Delete</a>" }
            ]
        });

    })
</script>