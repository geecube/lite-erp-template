<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Applicants</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="HR-recruitment-applicants-list.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	    	<div class="row">
	            <div class="col-md-10">
	                <div class="ibox">
	                    <div class="ibox-head">
	                        <div class="ibox-title">New Applicant</div>
	                        <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                            <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
	                            <div class="dropdown-menu dropdown-menu-right">
	                                <a class="dropdown-item">option 1</a>
	                                <a class="dropdown-item">option 2</a>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="ibox-body">
	                        <form>
	                        	<div class="row">
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Applying As</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Job Advertisement Found</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
	                        		<div class="col-sm-12">
										<h5 class="text-uppercase text-primary">
											<span>Personal Information</span>
										</h5>
									</div>
	                        	</div>
	                        	<hr>
								<div class="row">
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>First Name</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Middle Name</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Last Name</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Street</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Municipality</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-3">
			                            <div class="form-group">
			                                <label>City</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Province</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>E-mail Address</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Contact Number</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Date of Birth</label>
			                                <input class="form-control" type="date">
			                            </div>
		                        	</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
	                        		<div class="col-sm-12">
										<h5 class="text-uppercase text-primary">
											<span>Education</span>
										</h5>
									</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
									<div class="col-sm-12 col-xs-8">
										<table class="table table-striped dynamic mb-0" id="schedule-table">
											<thead class="bg-primary text-light">
												<th>Educational Attainment</th>
												<th>School/Univeristy Attended</th>
												<th>Year</th>
												<th></th>
											</thead>
											<tbody>
												<tr>
													<td>
														<select class="form-control">
															<option selected="">Select Type</option>
															<option>Elementary</option>
															<option>Highschool</option>
															<option>Undergraduate</option>
															<option>Graduate</option>
														</select>
													</td>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<button type="button" class="btn btn-danger remove-line btn-sm"><i class="fa fa-times"></i></button>
													</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="3">
														<button type="button" class="btn btn-success btn-sm addDatePicker-line"><i class="fa fa-plus"></i> New line</button>
													</td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
								<hr>
								<div class="row">
	                        		<div class="col-sm-12">
										<h5 class="text-uppercase text-primary">
											<span>Previous Employment</span>
										</h5>
									</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
									<div class="col-sm-12 col-xs-8">
										<table class="table table-striped dynamic mb-0" id="schedule-table">
											<thead class="bg-primary text-light">
												<th>Previous Employer</th>
												<th>Previous Position</th>
												<th>Salary</th>
												<th></th>
											</thead>
											<tbody>
												<tr>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<button type="button" class="btn btn-danger remove-line btn-sm"><i class="fa fa-times"></i></button>
													</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="3">
														<button type="button" class="btn btn-success btn-sm addDatePicker-line"><i class="fa fa-plus"></i> New line</button>
													</td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
								<hr>
								<div class="row">
	                        		<div class="col-sm-12">
										<h5 class="text-uppercase text-primary">
											<span>Salary</span>
										</h5>
									</div>
	                        	</div>
								<hr>
								<div class="row">
	                        		<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Basic Salary</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Allowance</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Expected Salary</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Negotiatied at</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
	                        	</div>
	                        	<hr>
								<div class="row">
	                        		<div class="col-sm-12">
										<h5 class="text-uppercase text-primary">
											<span>Others</span>
										</h5>
									</div>
	                        	</div>
								<hr>
								<div class="row">
									<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Availability for Employment</label>
			                                <input class="form-control" type="date">
			                            </div>
		                        	</div>
		                        	<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Remarks</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
								</div>
	                        	<hr>
	                            <div class="form-group">
	                                <button class="btn btn-success"><i class="fa fa-save"></i> Save</button>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>