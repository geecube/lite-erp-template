<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Leave Request</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="nav-HR-timekeeping.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	    	<div class="row">
	            <div class="col-md-10">
	                <div class="ibox">
	                    <div class="ibox-head">
	                        <div class="ibox-title">Leave Request</div>
	                        <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                        </div>
	                    </div>
	                    <div class="ibox-body">
	                        <form>
	                        	<div class="row">
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label><b>Employee Name</b></label>
			                                <p>Test</p>
			                            </div>
		                        	</div>
		                        	<div class="col-md-4">
			                            <div class="form-group">
			                                <label><b>Date Filed</b></label>
			                                <p>Test</p>
			                            </div>
		                        	</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
	                        		<div class="col-sm-12">
										<h5 class="text-uppercase text-primary">
											<span>Leave Details</span>
										</h5>
									</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
									<div class="col-sm-12 col-xs-8">
										<table class="table table-striped dynamic mb-0" id="schedule-table">
											<thead class="bg-primary text-light">
												<th>Leave Type *</th>
												<th>From *</th>
												<th>To *</th>
												<th colspan="2">Reason for Leave *</th>
											</thead>
											<tbody>
												<tr>
													<td>
														<p>Vacation Leave</p>
													</td>
													<td>
														<p>0000-00-00</p>
													</td>
													<td>
														<p>0000-00-00</p>
													</td>
													<td>
														<p>Test</p>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<hr>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
			                                <label><b>Approved By</b></label>
			                                <p>Test</p>
			                            </div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
			                                <label><b>Date Approved</b></label>
			                                <p>0000-00-00</p>
			                            </div>
									</div>
								</div>
								<hr>
	                            <div class="form-group">
	                                <a href="HR-timekeeping-leave-request-edit.php" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>