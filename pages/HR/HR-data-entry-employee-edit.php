<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Employee</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="HR-data-entry-employee.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	    	<div class="row">
	            <div class="col-md-9">
	                <div class="ibox">
	                    <div class="ibox-head">
	                        <div class="ibox-title">Edit Employee</div>
	                        <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                        </div>	
	                    </div>
	                    <div class="ibox-body">
	                        <form>
	                        	<h4 class="header-title m-t-0 m-b-20"><b>Details</b></h4>
	                        	<hr>
	                        	<div class="row">
	                        		<div class="col-md-4">
	                        			<div class="form-group">
			                                <label>Employment Status</label>
			                                <select class="form-control">
			                                	<option>Select Status</option>
			                                	<option selected="">Contractual</option>
			                                	<option>Expatriates</option>
			                                	<option>Probationary</option>
			                                	<option>Project Based</option>
			                                	<option>Regular</option>
			                                </select>
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Date Hired</label>
			                                <input class="form-control" type="text" value="2018-07-18">
			                            </div>
	                        		</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-4">
	                        			<div class="form-group">
			                                <label>Division</label>
			                                <input class="form-control" type="text" value="test">
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Branch</label>
			                                <input class="form-control" type="text" value="test">
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Department</label>
			                                <input class="form-control" type="text" value="test">
			                            </div>
	                        		</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-4">
	                        			<div class="form-group">
			                                <label>Position</label>
			                                <input class="form-control" type="text" value="test">
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Schedule</label>
			                                <input class="form-control" type="text" value="test">
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Office Assignment</label>
			                                <input class="form-control" type="text" value="test">
			                            </div>
	                        		</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-4">
	                        			<div class="form-group">
			                                <label class="text-right">Basic Salary</label>
			                                <input class="form-control" type="text" value="test">
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Overtime Pay?</label>
			                                <input class="form-control" type="text" value="test">
			                            </div>
	                        		</div>
	                        	</div>
	                        	<h4 class="header-title m-t-0 m-b-20"><b>Profile</b></h4>
	                        	<hr>
	                        	<div class="row">
	                        		<div class="col-md-4">
	                        			<div class="form-group">
			                                <label>First Name</label>
			                                <input class="form-control" type="text" value="John">
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Middle Name</label>
			                                <input class="form-control" type="text">
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Last Name</label>
			                                <input class="form-control" type="text" value="Doe">
			                            </div>
	                        		</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-3">
	                        			<div class="form-group">
			                                <label>Nickname</label>
			                                <input class="form-control" type="text" value="John">
			                            </div>
	                        		</div>
	                        		<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Birthday</label>
			                                <input class="form-control" type="text" value="1990-07-15">
			                            </div>
	                        		</div>
	                        		<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Gender</label>
			                                <select class="form-control">
			                                	<option>Select Gender</option>
			                                	<option selected="">Male</option>
			                                	<option>Female</option>
			                                </select>
			                            </div>
	                        		</div>
	                        		<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Civil Status</label>
			                                <select class="form-control">
			                                	<option>Select Status</option>
			                                	<option selected="">Single</option>
			                                	<option>Married</option>
			                                	<option>Widowed</option>
			                                </select>
			                            </div>
	                        		</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-6">
	                        			<div class="form-group">
			                                <label>Present Address</label>
			                                <textarea class="form-control">Test</textarea>
			                            </div>
	                        		</div>
	                        		<div class="col-md-6">
			                            <div class="form-group">
			                                <label>Provincial Address</label>
			                                <textarea class="form-control">Test</textarea>
			                            </div>
	                        		</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-3">
	                        			<div class="form-group">
			                                <label>TIN No.</label>
			                                <input class="form-control" type="text" value="None"> 
			                            </div>
	                        		</div>
	                        		<div class="col-md-3">
			                            <div class="form-group">
			                                <label>SSS No.</label>
			                                <input class="form-control" type="text" value="None">
			                            </div>
	                        		</div>
	                        		<div class="col-md-3">
			                            <div class="form-group">
			                                <label>PHILHEALTH No.</label>
			                                <input class="form-control" type="text" value="None">
			                            </div>
	                        		</div>
	                        		<div class="col-md-3">
			                            <div class="form-group">
			                                <label>PAG-IBIG No.</label>
			                                <input class="form-control" type="text" value="None">
			                            </div>
	                        		</div>
	                        	</div>
	                            <div class="form-group">
	                                <a href="HR-data-entry-employee.php" class="btn btn-success"><i class="fa fa-pencil"></i> Edit</a>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>