<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-heading">
            <h1 class="page-title">Schedule Type</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="HR-maintainables-schedule-type-list.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
        <div class="page-content fade-in-up">
            <div class="row">
                <div class="col-md-6">
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Edit Schedule Type</div>
                            <div class="ibox-tools">
                                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                        </div>
                        <div class="ibox-body">
                            <form>
                                <div class="row">
	                        		<div class="col-md-12">
			                            <div class="form-group">
			                                <label>Schedule Type</label>
			                                <input class="form-control" type="text" value="Test">
			                            </div>
		                        	</div>
	                        	</div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5 class="text-uppercase text-primary">
                                            <span>Schedules</span>
                                        </h5>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12 col-xs-8">
                                        <table class="table table-striped dynamic mb-0" id="schedule-table">
                                            <thead class="bg-primary text-light">
                                                <th></th>
                                                <th class="text-center">Day</th>
                                                <th class="text-center">Time In</th>
                                                <th class="text-center">Time Out</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox">
                                                    </td>
                                                    <td class="text-center">
                                                        Sunday
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" checked="">
                                                    </td>
                                                    <td class="text-center">
                                                        Monday
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="0:00 AM">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="0:00 PM">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" checked="">
                                                    </td>
                                                    <td class="text-center">
                                                        Tuesday
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="0:00 AM">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="0:00 PM">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" checked="">
                                                    </td>
                                                    <td class="text-center">
                                                        Wednesday
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="0:00 AM">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="0:00 PM">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" checked="">
                                                    </td>
                                                    <td class="text-center">
                                                        Thursday
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="0:00 AM">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="0:00 PM">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" checked="">
                                                    </td>
                                                    <td class="text-center">
                                                        Friday
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="0:00 AM">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="0:00 PM">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox">
                                                    </td>
                                                    <td class="text-center">
                                                        Saturday
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control">
                                                    </td>
                                                </tr>
                                            </tbody> 
                                        </table>
                                    </div>
                                </div>
	                        	<hr>
                                <div class="form-group">
                                    <button class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
        
<?php include '../../layouts/footer.php' ?>
