<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Payrolls</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="nav-HR-payroll.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	    	<div class="row">
	            <div class="col-sm-12">
	                <div class="ibox">
	                    <div class="ibox-head">
	                        <div class="ibox-title">Generate Payroll</div>
	                        <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                            <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
	                        </div>
	                    </div>
	                    <div class="ibox-body">
	                        <form>
	                        	<div class="row">
	                        		<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Department</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
		                        	<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Date From</label>
			                                <input class="form-control" type="date">
			                            </div>
		                        	</div>
		                        	<div class="col-md-3">
			                            <div class="form-group">
			                                <label>Date To</label>
			                                <input class="form-control" type="date">
			                            </div>
		                        	</div>
		                        	<div class="col-md-3">
			                            <div class="form-group">
			                            	<label>Action</label>
			                                <button class="btn btn-success form-control">Generate</button>
			                            </div>
		                        	</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
									<div class="col-lg-12 col-xs-8">
										<div class="table-responsive table-responsive-lg">
											<table class="table table-bordered table-sm">
												<thead>
													<tr>
														<th></th>
														<th colspan="2" class="text-center">Employee Info.</th>
														<th colspan="6" class="text-center bg-warning text-white">Attendance Report</th>
														<th colspan="3" class="text-center bg-info text-white">Salaries Rate</th>
														<th colspan="2" class="text-center bg-danger text-white">Tardiness</th>
														<th class="bg-success text-white"></th>
														<th colspan="2" class="text-center bg-primary text-white">Overtime Pay</th>
														<th class="bg-success text-white"></th>
														<th colspan="6" class="text-center bg-danger text-white">Deductions</th>
														<th class="bg-success text-white"></th>
													</tr>
													<tr>
														<th>No.</th>
														<th>Employee</th>
														<th>Position</th>
														<th class="bg-warning text-white">Reg. Days</th>
														<th class="bg-warning text-white">Acq. Days</th>
														<th class="bg-warning text-white">Reg. Hours</th>
														<th class="bg-warning text-white">Acq. Hours</th>
														<th class="bg-warning text-white">Late mins</th>
														<th class="bg-warning text-white">OT Hours</th>
														<th class="text-right bg-info text-white">Monthly</th>
														<th class="text-right bg-info text-white">Daily</th>
														<th class="text-right bg-info text-white">Reg. Salaries</th>
														<th class="text-right bg-danger text-white">Late</th>
														<th class="text-right bg-danger text-white">Absent</th>
														<th class="text-right bg-success text-white">Net Salaries</th>
														<th class="text-right bg-primary text-white">Reg. OT</th>
														<th class="text-right bg-primary text-white">NSD</th>
														<th class="text-right bg-success text-white">Gross Pay</th>
														<th class="text-right bg-danger text-white">SSS</th>
														<th class="text-right bg-danger text-white">HDMF</th>
														<th class="text-right bg-danger text-white">PHIC</th>
														<th class="text-right bg-danger text-white">Loan</th>
														<th class="text-right bg-danger text-white">C.A</th>
														<th class="text-right bg-danger text-white">Others</th>
														<th class="text-right bg-success text-white">Net Pay</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td colspan="25" class="text-center">No Data Generated</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- <div class="row">
	            <div class="col-md-10">
	            	<div class="ibox">
	        		                <div class="ibox-head">
	        		                    <div class="ibox-title">Payroll List</div>
	        		                    <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                            <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
	                            <div class="dropdown-menu dropdown-menu-right">
	                                <a class="dropdown-item">option 1</a>
	                                <a class="dropdown-item">option 2</a>
	                            </div>
	                        </div>
	        		                </div>
	        		                <div class="ibox-body">
	        		                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
	        		                        <thead>
	        		                            <tr>
	        		                                <th>Employee Name</th>
	        		                                <th>Position</th>
	        		                                <th>Department</th>
	        		                                <th>Action</th>
	        		                            </tr>
	        		                        </thead>
	        		                        <tbody>
	        		                            <tr>
	        		                                <td>John Doe</td>
	        		                                <td></td>
	        		                                <td></td>
	        		                                <td>
	        		                                	<a href="#" class="btn btn-outline-info btn-sm">
	        											    <i class="ti-pencil"></i> Edit
	        											</a>
	        										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
	        										        <span class="ti-trash"></span> Delete
	        										    </a>
	        										</td>
	        		                            </tr>
	        		                            <tr>
	        		                                <td>Jane Doe</td>
	        		                                <td></td>
	        		                                <td></td>
	        		                                <td>
	        		                                	<a href="#" class="btn btn-outline-info btn-sm">
	        											    <i class="ti-pencil"></i> Edit
	        											</a>
	        										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
	        										        <span class="ti-trash"></span> Delete
	        										    </a>
	        										</td>
	        		                            </tr>
	        		                        </tbody>
	        		                    </table>
	        		                </div>
	        		            </div>
	            </div>
	                	</div> -->
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>