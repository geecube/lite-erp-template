<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Project Assignment</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="nav-HR-projects.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
                &emsp;
                <a href="HR-projects-assignment-add.php" class="btn btn-success btn-md"><i class="fa fa-plus"></i> Create Assignment</a>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	        <div class="row">
	            <div class="col-md-12">
	            	<div class="ibox">
		                <div class="ibox-head">
		                    <div class="ibox-title">Project Assignment List</div>
		                    <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                        </div>
		                </div>
		                <div class="ibox-body table-responsive">
		                    <table class="table table-striped table-bordered table-hover" id="project-assignment-table" cellspacing="0" width="100%">
		                        <thead>
		                            <tr>
		                                <th>Reference Number</th>
		                                <th>Project Name</th>
		                                <th>Project Start</th>
		                                <th>Estimate Project Finish</th>
		                                <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <!-- <tr>
		                                <td>John Doe</td>
		                                <td></td>
		                                <td></td>
		                                <td></td>
		                                <td>
		                                	<a href="#" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> Edit
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr>
		                            <tr>
		                                <td>Jane Doe</td>
		                                <td></td>
		                                <td></td>
		                                <td></td>
		                                <td>
		                                	<a href="#" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> Edit
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr> -->
		                        </tbody>
		                    </table>
		                </div>
		            </div>
	            </div>
        	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>

<script type="text/javascript">
    jQuery(document).ready(function ($) {

        $('#project-assignment-table').DataTable({
            pageLength: 10,
            "ajax": '../../assets/data/HR/table_project_assignment.json',
            "columns": [
                { "data": "reference" },
                { "data": "title" },
                { "data": "start" },
                { "data": "finish" },
                { "defaultContent": "<a href=\"HR-projects-assignment-edit.php\" class=\"btn btn-outline-info btn-sm\" \/><i class=\"ti-pencil\"></i> Edit</a>" +    ' ' + "<a href=\"#\" class=\"btn btn-outline-danger btn-sm\" \/><i class=\"ti-trash\"></i> Delete</a>" }
            ]
        });

    })
</script>