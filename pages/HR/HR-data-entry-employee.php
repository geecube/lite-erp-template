<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Employee</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="HR-data-entry-employee-list.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	    	<div class="row">
	            <div class="col-md-9">
	                <div class="ibox">
	                    <div class="ibox-head">
	                        <div class="ibox-title">Add Employee</div>
	                        <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                            <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
	                        </div>
	                    </div>
	                    <div class="ibox-body">
	                        <form>
	                        	<h4 class="header-title m-t-0 m-b-20"><b>Details</b></h4>
	                        	<hr>
	                        	<div class="row">
	                        		<div class="col-md-4">
	                        			<div class="form-group">
			                                <label><b>Employment Status</b></label>
			                                <p>Contractual</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label><b>Date Hired</b></label>
			                                <p>2018-07-18</p>
			                            </div>
	                        		</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-4">
	                        			<div class="form-group">
			                                <label><b>Division</b></label>
			                                <p>Test</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label><b>Branch</b></label>
			                                <p>Test</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label><b>Department</b></label>
			                                <p>Test</p>
			                            </div>
	                        		</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-4">
	                        			<div class="form-group">
			                                <label><b>Position</b></label>
			                                <p>Test</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label><b>Schedule</b></label>
			                                <p>Test</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label><b>Office Assignment</b></label>
			                                <p>Test</p>
			                            </div>
	                        		</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-4">
	                        			<div class="form-group">
			                                <label class="text-right"><b>Basic Salary</b></label>
			                                <p>Test</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label><b>Overtime Pay?</b></label>
			                                <p>Test</p>
			                            </div>
	                        		</div>
	                        	</div>
	                        	<h4 class="header-title m-t-0 m-b-20"><b>Profile</b></h4>
	                        	<hr>
	                        	<div class="row">
	                        		<div class="col-md-4">
	                        			<div class="form-group">
			                                <label><b>Name</b></label>
			                                <p>John Doe</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
	                        			<div class="form-group">
			                                <label><b>Nickname</b></label>
			                                <p>John</p>
			                            </div>
	                        		</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label><b>Birthday</b></label>
			                                <p>1990-07-15</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label><b>Gender</b></label>
			                                <p>Male</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label><b>Civil Status</b></label>
			                                <p>Single</p>
			                            </div>
	                        		</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-6">
	                        			<div class="form-group">
			                                <label><b>Present Address</b></label>
			                                <p>Test</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-6">
			                            <div class="form-group">
			                                <label><b>Provincial Address</b></label>
			                                <p>Test</p>
			                            </div>
	                        		</div>
	                        	</div>
	                        	<div class="row">
	                        		<div class="col-md-3">
	                        			<div class="form-group">
			                                <label><b>TIN No.</b></label>
			                                <p>None</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-3">
			                            <div class="form-group">
			                                <label><b>SSS No.</b></label>
			                                <p>None</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-3">
			                            <div class="form-group">
			                                <label><b>PHILHEALTH No.</b></label>
			                                <p>None</p>
			                            </div>
	                        		</div>
	                        		<div class="col-md-3">
			                            <div class="form-group">
			                                <label><b>PAG-IBIG No.</b></label>
			                                <p>None</p>
			                            </div>
	                        		</div>
	                        	</div>
	                        	<hr>
	                            <div class="form-group">
	                                <a href="HR-data-entry-employee-edit.php" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {

	    $('#employee-table').DataTable({
	        pageLength: 10,
	        "ajax": './assets/demo/data/table_employee.json',
	        "columns": [
	            { "data": "id_no" },
	            { "data": "name" },
	            { "data": "position" },
	            { "data": "department" },
	            { "data": "division" },
	            { "data": "branch" },
	            { "defaultContent": "<a href=\"#\" class=\"btn btn-outline-info btn-sm\" \/><i class=\"ti-pencil\"></i> View</a>" +	' ' + "<a href=\"#\" class=\"btn btn-outline-danger btn-sm\" \/><i class=\"ti-trash\"></i> Delete</a>" }
	        ]
	    });

	})
</script>