<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-content fade-in-up">
	    	<br>
	    	<div class="row">
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-data-entry-position-list.php" class="lg"><i class="fa fa-building lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Position</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-data-entry-division-list.php" class="lg"><i class="fa fa-building lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Division</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-data-entry-branch-list.php" class="lg"><i class="fa fa-building lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Branch</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    	<br><br>	
	    	<div class="row">
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-data-entry-department-list.php" class="lg"><i class="fa fa-building lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Department</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-4">
	    			<div class="col-md-12">
	    				<center><a href="HR-data-entry-employee-list.php" class="lg"><i class="fa fa-address-card lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Employee</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>