<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Applicants</h1>
		                    	
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="../../index.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
                &emsp;
                <a href="HR-recruitment-applicant-add.php" class="btn btn-success btn-md"><i class="fa fa-plus"></i> Add Applicant</a>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	        <div class="row">
	            <div class="col-md-12">
	            	<div class="ibox">
		                <div class="ibox-head">
		                    <div class="ibox-title">Applicant List</div>
		                    <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                        </div>
		                </div>
		                <div class="ibox-body table-responsive">
		                    <table class="table table-striped table-bordered table-hover" id="applicant-table" cellspacing="0" width="100%">
		                        <thead>
		                            <tr>
		                                <th>Date Applied</th>
										<th>Name</th>
										<th>Age</th>
										<th>Email Address</th>
										<th>Contact Number</th>
										<th>Applied As</th>
										<th>Status</th>
										<th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <!-- <tr>
		                                <td>07/17/2018</td>
		                                <td>John Doe</td>
		                                <td>29</td>
		                                <td>johndoe@gmail.com</td>
		                                <td>09231434156</td>
		                                <td>Quantity Surveyor</td>
		                                <td>Pending</td>
		                                <td>
		                                	<a href="HR-recruitment-applicant.php" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> View
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr>
		                            <tr>
		                            	<td></td>
		                                <td>Jane Doe</td>
		                                <td></td>
		                                <td></td>
		                                <td></td>
		                                <td></td>
		                                <td></td>
		                                <td>
		                                	<a href="#" class="btn btn-outline-info btn-sm">
											    <i class="ti-pencil"></i> View
											</a>
										  	<a class="btn btn-outline-danger btn-sm trash-row" href="#">
										        <span class="ti-trash"></span> Delete
										    </a>
										</td>
		                            </tr> -->
		                        </tbody>
		                    </table>
		                </div>
		            </div>
	            </div>
        	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {

	    $('#applicant-table').DataTable({
	        pageLength: 10,
	        "ajax": '../../assets/data/HR/table_applicants.json',
	        "columns": [
	            { "data": "date_applied" },
	            { "data": "name" },
	            { "data": "age" },
	            { "data": "email" },
	            { "data": "contact" },
	            { "data": "applied_as" },
	            { "data": "status" },
	            { "defaultContent": "<a href=\"HR-recruitment-applicant.php\" class=\"btn btn-outline-info btn-sm\" \/><i class=\"ti-pencil\"></i> View</a>" +	' ' + "<a href=\"#\" class=\"btn btn-outline-danger btn-sm\" \/><i class=\"ti-trash\"></i> Delete</a>" }
	        ]
	    });

	})
</script>