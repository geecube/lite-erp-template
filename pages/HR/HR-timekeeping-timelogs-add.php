<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-heading">
            <h1 class="page-title">Timelogs</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="HR-timekeeping-timelogs-list.php"><i class="fa fa-arrow-left font-20"></i></a>
                </li>
                <li class="breadcrumb-item">Menu</li>
            </ol>
        </div>
	    <div class="page-content fade-in-up">
	    	<div class="row">
	            <div class="col-md-10">
	                <div class="ibox">
	                    <div class="ibox-head">
	                        <div class="ibox-title">Add Timelog</div>
	                        <div class="ibox-tools">
	                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
	                            <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
	                        </div>
	                    </div>
	                    <div class="ibox-body">
	                        <form>
	                        	<div class="row">
	                        		<div class="col-md-4">
			                            <div class="form-group">
			                                <label>Employee Name</label>
			                                <input class="form-control" type="text">
			                            </div>
		                        	</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
	                        		<div class="col-sm-12">
										<h5 class="text-uppercase text-primary">
											<span>Timelog Details</span>
										</h5>
									</div>
	                        	</div>
	                        	<hr>
	                        	<div class="row">
									<div class="col-sm-12 col-xs-8 table-responsive">
										<table class="table table-striped table-bordered mb-0 dynamic" id="schedule-table">
											<thead>
												<tr>
							                        <th colspan="2" class="bg-primary text-white text-center">TIME-IN</th>
							                        <th colspan="2" class="bg-secondary text-white text-center">TIME-OUT</th>
							                        <th class="bg-warning text-white text-center">OTHERS</th>
							                        <th></th>
							                    </tr>
												<tr>
													<th class="text-center">Date</th>
													<th class="text-center">Time</th>
													<th class="text-center">Date</th>
													<th class="text-center">Time</th>
													<th class="text-center">Approved Overtime?</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<input type="date" class="form-control">
													</td>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<input type="date" class="form-control">
													</td>
													<td>
														<input type="text" class="form-control">
													</td>
													<td>
														<input type="checkbox" class="form-control">
													</td>
													<td>
														<button type="button" class="btn btn-danger remove-line btn-sm"><i class="fa fa-times"></i></button>
													</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="6">
														<button type="button" class="btn btn-success btn-sm addDatePicker-line"><i class="fa fa-plus"></i> New line</button>
													</td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
								<hr>
	                            <div class="form-group">
	                                <button class="btn btn-success"><i class="fa fa-save"></i> Save</button>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include 'layouts/footer.php' ?>