<?php include '../../layouts/header.php' ?>

<?php include '../../navigation/navbar.php' ?>

<?php include '../../navigation/sidenav.php' ?>

	<div class="content-wrapper">
	    <!-- START PAGE CONTENT-->
	    <div class="page-content fade-in-up">
	    	<br>
	    	<div class="row">
	    		<div class="col-md-6">
	    			<div class="col-md-12">
	    				<center><a href="HR-projects-master-list.php" class="lg"><i class="fa fa-archive lg"></i></a></center>
	    			</div>
	    			<div class="col-md-12">
			    		<center><h4>Project Master List</h4></center>
			    	</div>
	    		</div>
	    		<div class="col-md-6">
	    			<div class="col-md-10">
	    				<center><a href="HR-projects-assignment-list.php" class="lg"><i class="fa fa-address-book lg"></i></a></center>
	    			</div>
	    			<div class="col-md-10">
			    		<center><h4>Project Assignment</h4></center>
			    	</div>
	    		</div>
	    	</div>
	    </div>
	    <!-- END PAGE CONTENT-->
	    
<?php include '../../layouts/footer.php' ?>